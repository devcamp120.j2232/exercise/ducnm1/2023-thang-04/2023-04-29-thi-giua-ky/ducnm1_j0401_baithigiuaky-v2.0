-- phpMyAdmin SQL Dump
-- version 5.2.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Generation Time: May 03, 2023 at 08:53 PM
-- Server version: 10.4.28-MariaDB
-- PHP Version: 8.2.4

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `school_db`
--

-- --------------------------------------------------------

--
-- Table structure for table `classroom`
--

CREATE TABLE `classroom` (
  `id` bigint(20) NOT NULL,
  `classroom_code` varchar(255) DEFAULT NULL,
  `classroom_name` varchar(255) DEFAULT NULL,
  `created_date` datetime DEFAULT NULL,
  `teacher_name` varchar(255) DEFAULT NULL,
  `teacher_phone_number` varchar(10) DEFAULT NULL,
  `updated_date` datetime DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `classroom`
--

INSERT INTO `classroom` (`id`, `classroom_code`, `classroom_name`, `created_date`, `teacher_name`, `teacher_phone_number`, `updated_date`) VALUES
(71, '10A2', '10A2', '2023-05-04 01:44:56', 'Trần Văn Bình', '0912345678', NULL),
(73, '10A3', '10A3', '2023-05-04 01:46:06', 'Nguyễn Mạnh Cường', '0912456456', NULL),
(76, '10A1', '10A1', '2023-05-04 01:47:35', 'Nguyễn Minh Anh', '0123456001', NULL);

-- --------------------------------------------------------

--
-- Table structure for table `hibernate_sequence`
--

CREATE TABLE `hibernate_sequence` (
  `next_val` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `hibernate_sequence`
--

INSERT INTO `hibernate_sequence` (`next_val`) VALUES
(80);

-- --------------------------------------------------------

--
-- Table structure for table `student`
--

CREATE TABLE `student` (
  `id` bigint(20) NOT NULL,
  `address` varchar(255) DEFAULT NULL,
  `birthday` bigint(20) NOT NULL,
  `created_date` bigint(20) DEFAULT NULL,
  `gender` varchar(255) DEFAULT NULL,
  `phone_number` varchar(10) DEFAULT NULL,
  `student_code` varchar(255) DEFAULT NULL,
  `student_name` varchar(255) DEFAULT NULL,
  `updated_date` bigint(20) DEFAULT NULL,
  `classroom_id` bigint(20) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_unicode_ci;

--
-- Dumping data for table `student`
--

INSERT INTO `student` (`id`, `address`, `birthday`, `created_date`, `gender`, `phone_number`, `student_code`, `student_name`, `updated_date`, `classroom_id`) VALUES
(75, 'Bắc Giang', 1700092800000, 1683139630070, 'Nam', '0973271279', '2', 'Nguyễn Quang Huy', 0, 71),
(77, 'Bắc Giang', 1680652800000, 1683139855687, 'Nam', '0979736090', '1', 'Nguyễn Minh Đức', 1683139861804, 76),
(78, 'Hải Phòng', 1571184000000, 1683139939672, 'Nữ', '0979736091', '3', 'Nguyễn Hương Giang', 0, 73),
(79, 'Hoàng Mai', 940032000000, 1683139976869, 'Nam', '0981888777', '4', 'Nguyễn Tuấn Hoàng', 0, 73);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `classroom`
--
ALTER TABLE `classroom`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_oac2s50vluny3nm5a3yt7s0cr` (`classroom_code`);

--
-- Indexes for table `student`
--
ALTER TABLE `student`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `UK_i3xrfnuv2icsd1vhvn6c108ec` (`phone_number`),
  ADD UNIQUE KEY `UK_pilb3uo1cimnf1sp86nqcrjsv` (`student_code`),
  ADD KEY `FK1rs4md9whkjqy20v181d18kfy` (`classroom_id`);

--
-- Constraints for dumped tables
--

--
-- Constraints for table `student`
--
ALTER TABLE `student`
  ADD CONSTRAINT `FK1rs4md9whkjqy20v181d18kfy` FOREIGN KEY (`classroom_id`) REFERENCES `classroom` (`id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
