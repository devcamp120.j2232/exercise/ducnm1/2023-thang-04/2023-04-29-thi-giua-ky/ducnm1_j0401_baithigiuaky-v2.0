package com.devcamp.baiThiGiuaKy.repository;

import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;

import com.devcamp.baiThiGiuaKy.models.Student;

public interface StudentRepository extends JpaRepository<Student, Long> {
    public boolean existsByPhoneNumber(String phoneNumber);

    public boolean existsByStudentCode(String studentCode);

    public Optional<Student> findByStudentCodeAndPhoneNumber(String studentCode, String phoneNumber);

    public Optional<Student> findByStudentCode(String studentCode);

    public Optional<Student> findByPhoneNumber(String phoneNumber);
}
