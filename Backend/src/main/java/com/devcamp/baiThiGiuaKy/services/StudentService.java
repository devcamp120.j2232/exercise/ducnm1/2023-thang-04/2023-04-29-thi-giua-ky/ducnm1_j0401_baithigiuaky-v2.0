package com.devcamp.baiThiGiuaKy.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.devcamp.baiThiGiuaKy.exceptions.DuplicateEntryException;
import com.devcamp.baiThiGiuaKy.models.Classroom;
import com.devcamp.baiThiGiuaKy.models.Student;
import com.devcamp.baiThiGiuaKy.repository.ClassroomRepository;
import com.devcamp.baiThiGiuaKy.repository.StudentRepository;

@Service
public class StudentService {
    @Autowired
    private StudentRepository studentRepository;
    @Autowired
    private ClassroomRepository classroomRepository;

    private Student saveStudent(Student student) throws Exception {
        if (studentRepository.existsByStudentCode(student.getStudentCode())) {
            throw new DuplicateEntryException("Mã học sinh " + student.getStudentCode() + " đã tồn tại");
        }

        if (studentRepository.existsByPhoneNumber(student.getPhoneNumber())) {
            throw new DuplicateEntryException("Số điện thoại " + student.getPhoneNumber() + " đã tồn tại");
        }

        return studentRepository.save(student);
    }

    public List<Student> getAllStudents() {
        List<Student> students = new ArrayList<Student>();
        studentRepository.findAll().forEach(students::add);
        return students;
    }

    public Student getStudentById(long id) {
        Optional<Student> student = studentRepository.findById(id);
        if (student.isPresent()) {
            return student.get();
        } else {
            return null;
        }
    }

    public Student createStudent(long classroomId, Student studentBody) throws Exception {
        Student newStudent = new Student();
        newStudent.setStudentCode(studentBody.getStudentCode());
        newStudent.setStudentName(studentBody.getStudentName());
        newStudent.setGender(studentBody.getGender());
        newStudent.setBirthday(studentBody.getBirthday());
        newStudent.setAddress(studentBody.getAddress());
        newStudent.setPhoneNumber(studentBody.getPhoneNumber());

        Classroom classroom = classroomRepository.findById(classroomId).get();
        newStudent.setClassroom(classroom);

        newStudent.setCreatedDate(new Date().getTime());

        return saveStudent(newStudent);
    }

    public Student updateStudentById(long id, long classroomId, Student studentBodyRequest) throws Exception {
        Optional<Student> optionalStudent = studentRepository.findById(id);
        if (!optionalStudent.isPresent()) {
            return null;
        }
        Student existingStudent = optionalStudent.get();

        // student code và student phone number là unique
        // kiểu tra student code và phone number từ request đưa vào
        Optional<Student> optionalFindStudentByStudentCode = studentRepository
                .findByStudentCode(studentBodyRequest.getStudentCode());
        if (optionalFindStudentByStudentCode.isPresent()) {
            if (existingStudent.getId() != optionalFindStudentByStudentCode.get().getId()) {// Nếu không trùng id, nghĩa
                                                                                            // là có 1 student khác có
                                                                                            // code này rồi
                throw new DuplicateEntryException("Đã tồn tại mã học sinh: " + studentBodyRequest.getStudentCode());
            }
        }

        Optional<Student> optionalFindStudentByPhoneNumber = studentRepository
                .findByPhoneNumber(studentBodyRequest.getPhoneNumber());
        if (optionalFindStudentByPhoneNumber.isPresent()) {
            if (existingStudent.getId() != optionalFindStudentByPhoneNumber.get().getId()) {// Nếu không trùng id, nghĩa
                                                                                            // là có 1 student khác có
                                                                                            // phone number này rồi
                throw new DuplicateEntryException("Đã tồn tại số điện thoại: " + studentBodyRequest.getPhoneNumber());
            }
        }

        // update vào chính student đã có
        existingStudent.setStudentCode(studentBodyRequest.getStudentCode());
        existingStudent.setStudentName(studentBodyRequest.getStudentName());
        existingStudent.setGender(studentBodyRequest.getGender());
        existingStudent.setBirthday(studentBodyRequest.getBirthday());
        existingStudent.setAddress(studentBodyRequest.getAddress());
        existingStudent.setPhoneNumber(studentBodyRequest.getPhoneNumber());

        Classroom classroom = classroomRepository.findById(classroomId).get();
        existingStudent.setClassroom(classroom);

        existingStudent.setUpdatedDate(new Date().getTime());

        return studentRepository.save(existingStudent);
    }

    public Student deleteStudentById(long id) {
        Optional<Student> student = studentRepository.findById(id);
        if (!student.isPresent()) {
            return null;
        }

        studentRepository.deleteById(id);
        return student.get();
    }

    public void deleteAllStudents() {
        studentRepository.deleteAll();
    }
}
