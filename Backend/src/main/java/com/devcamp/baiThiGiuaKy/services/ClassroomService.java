package com.devcamp.baiThiGiuaKy.services;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Optional;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.stereotype.Service;

import com.devcamp.baiThiGiuaKy.exceptions.DuplicateEntryException;
import com.devcamp.baiThiGiuaKy.models.Classroom;
import com.devcamp.baiThiGiuaKy.repository.ClassroomRepository;

@Service
public class ClassroomService {
    @Autowired
    private ClassroomRepository classroomRepository;

    public boolean existsByClassroomCode(String classroomCode) {
        return classroomRepository.existsByClassroomCode(classroomCode);
    }

    public Classroom saveClassroom(Classroom classroom) throws Exception {
        try {
            return classroomRepository.save(classroom);
        } catch (DataIntegrityViolationException e) {
            throw new DuplicateEntryException("Mã lớp học " + classroom.getClassroomCode() + " đã tồn tại");
        } catch (Exception e) {
            throw new Exception(e.getMessage());
        }
    }

    public List<Classroom> getAllClassrooms() {
        List<Classroom> classrooms = new ArrayList<Classroom>();
        classroomRepository.findAll().forEach(classrooms::add);
        return classrooms;
    }

    public Classroom getClassroomById(long id) {
        Optional<Classroom> classroom = classroomRepository.findById(id);
        if (classroom.isPresent()) {
            return classroom.get();
        } else {
            return null;
        }
    }

    public Classroom createClassroom(Classroom classroomBody) throws Exception {
        Classroom newClassroom = new Classroom();
        newClassroom.setClassroomCode(classroomBody.getClassroomCode());
        newClassroom.setClassroomName(classroomBody.getClassroomName());
        newClassroom.setTeacherName(classroomBody.getTeacherName());
        newClassroom.setTeacherPhoneNumber(classroomBody.getTeacherPhoneNumber());
        newClassroom.setStudents(classroomBody.getStudents());
        newClassroom.setCreatedDate(new Date());

        return saveClassroom(newClassroom);
    }

    public Classroom updateClassroomById(long id, Classroom classroomBodyRequest) throws Exception {
        Optional<Classroom> classroom = classroomRepository.findById(id);
        if (!classroom.isPresent()) {
            return null;
        }

        Classroom updatedClassroom = classroom.get();
        updatedClassroom.setClassroomCode(classroomBodyRequest.getClassroomCode());
        updatedClassroom.setClassroomName(classroomBodyRequest.getClassroomName());
        updatedClassroom.setTeacherName(classroomBodyRequest.getTeacherName());
        updatedClassroom.setTeacherPhoneNumber(classroomBodyRequest.getTeacherPhoneNumber());
        updatedClassroom.setStudents(classroomBodyRequest.getStudents());
        updatedClassroom.setUpdatedDate(new Date());

        return saveClassroom(updatedClassroom);
    }

    public Classroom deleteClassroomById(long id) {
        Optional<Classroom> classroom = classroomRepository.findById(id);
        if (!classroom.isPresent()) {
            return null;
        }

        classroomRepository.deleteById(id);
        return classroom.get();
    }

    public void deleteAllClassrooms() {
        classroomRepository.deleteAll();
    }
}
