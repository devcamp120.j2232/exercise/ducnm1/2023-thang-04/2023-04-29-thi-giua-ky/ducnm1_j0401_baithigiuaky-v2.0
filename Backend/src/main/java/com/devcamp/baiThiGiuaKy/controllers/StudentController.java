package com.devcamp.baiThiGiuaKy.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.baiThiGiuaKy.models.Student;
import com.devcamp.baiThiGiuaKy.services.StudentService;

@RestController
@CrossOrigin
@RequestMapping("")
public class StudentController {
    @Autowired
    private StudentService studentService;

    @GetMapping("/students")
    public ResponseEntity<List<Student>> getAllStudents() {
        try {
            return new ResponseEntity<>(studentService.getAllStudents(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @GetMapping("/students/{id}")
    public ResponseEntity<Student> getStudentById(@PathVariable long id) {
        try {
            Student student = studentService.getStudentById(id);
            if (student != null) {
                return new ResponseEntity<>(studentService.getStudentById(id), HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/students/create/{classroomId}")
    public ResponseEntity<Object> createStudent(@PathVariable long classroomId,
            @Valid @RequestBody Student studentBodyRequest) {
        try {
            Student newStudent = studentService.createStudent(classroomId, studentBodyRequest);
            return new ResponseEntity<>(newStudent, HttpStatus.CREATED);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/students/update/{id}/{classroomId}")
    public ResponseEntity<Object> updateStudentById(@PathVariable long id, @PathVariable long classroomId,
            @Valid @RequestBody Student studentBodyRequest) {
        try {
            Student student = studentService.getStudentById(id);
            if (student == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            Student updatedStudent = studentService.updateStudentById(id, classroomId, studentBodyRequest);
            return new ResponseEntity<>(updatedStudent, HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/students/delete/{id}")
    public ResponseEntity<Object> deleteStudentById(@PathVariable long id) {
        try {
            Student student = studentService.deleteStudentById(id);
            if (student == null) {
                return new ResponseEntity<Object>("Student with id " + id + " does not exist", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<Object>(HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/students/delete")
    public ResponseEntity<Object> deleteAllStudents() {
        try {
            studentService.deleteAllStudents();
            return new ResponseEntity<Object>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
