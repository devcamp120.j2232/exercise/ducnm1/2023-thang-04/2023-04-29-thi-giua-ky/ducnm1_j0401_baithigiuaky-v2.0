package com.devcamp.baiThiGiuaKy.controllers;

import java.util.List;

import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.baiThiGiuaKy.exceptions.DuplicateEntryException;
import com.devcamp.baiThiGiuaKy.models.Classroom;
import com.devcamp.baiThiGiuaKy.services.ClassroomService;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;

@RestController
@CrossOrigin
@RequestMapping("")
public class ClassroomController {
    @Autowired
    private ClassroomService classroomService;

    /**
     * @return all classrooms
     */
    @GetMapping("/classrooms")
    public ResponseEntity<List<Classroom>> getAllClassrooms() {
        try {
            return new ResponseEntity<>(classroomService.getAllClassrooms(), HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    /**
     * Retrieves a classroom by ID.
     *
     * @param id the ID of the classroom to retrieve
     * @return a ResponseEntity containing the classroom if it exists, or a null
     *         value with a 404 Not Found status code if it does not exist
     * @throws Exception if an error occurs while retrieving the classroom
     */
    @GetMapping("/classrooms/{id}")
    public ResponseEntity<Classroom> getClassroomById(@PathVariable long id) {
        try {
            Classroom classroom = classroomService.getClassroomById(id);
            if (classroom != null) {
                return new ResponseEntity<>(classroom, HttpStatus.OK);
            } else {
                return new ResponseEntity<>(null, HttpStatus.NOT_FOUND);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PostMapping("/classrooms/create")
    public ResponseEntity<Object> createClassroom(@Valid @RequestBody Classroom classroomBody) {
        try {
            Classroom newClassroom = classroomService.createClassroom(classroomBody);
            return new ResponseEntity<>(newClassroom, HttpStatus.CREATED);
        } catch (DuplicateEntryException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @PutMapping("/classrooms/update/{id}")
    public ResponseEntity<Object> updateClassroomById(@PathVariable long id,
            @Valid @RequestBody Classroom classroomBodyRequest) {
        try {
            Classroom classroom = classroomService.getClassroomById(id);
            if (classroom == null) {
                return new ResponseEntity<>(HttpStatus.NOT_FOUND);
            }

            Classroom updatedClassroom = classroomService.updateClassroomById(id, classroomBodyRequest);
            return new ResponseEntity<>(updatedClassroom, HttpStatus.OK);
        } catch (DuplicateEntryException e) {
            return new ResponseEntity<>(e.getMessage(), HttpStatus.CONFLICT);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/classrooms/delete/{id}")
    public ResponseEntity<Object> deleteClassroomById(@PathVariable long id) {
        try {
            Classroom classroom = classroomService.deleteClassroomById(id);
            if (classroom == null) {
                return new ResponseEntity<Object>("Classroom with id " + id + " does not exist", HttpStatus.NOT_FOUND);
            } else {
                return new ResponseEntity<Object>(HttpStatus.OK);
            }
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

    @DeleteMapping("/classrooms/delete")
    public ResponseEntity<Object> deleteAllClassrooms() {
        try {
            classroomService.deleteAllClassrooms();
            return new ResponseEntity<Object>(HttpStatus.OK);
        } catch (Exception e) {
            return new ResponseEntity<>(null, HttpStatus.INTERNAL_SERVER_ERROR);
        }
    }

}
