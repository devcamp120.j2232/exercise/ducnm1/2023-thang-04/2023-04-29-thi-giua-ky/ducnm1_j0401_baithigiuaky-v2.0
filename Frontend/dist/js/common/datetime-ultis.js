export function convertDateToHTML5String(date) {
    // Date control in HTML 5 accepts in the format of Year - month - day as we use in SQL
    // If the month is 9, it needs to be set as 09 not 9 simply. So it applies for day field also.

    var day = ("0" + date.getDate()).slice(-2);
    var month = ("0" + (date.getMonth() + 1)).slice(-2);
    return date.getFullYear() + "-" + (month) + "-" + (day);
}

export function formatDate(dateLong, format) {
    //"dd/MM/yyyy"
    if (dateLong == 0) return "";

    let date = new Date(dateLong);

    // console.log("------------------");
    // console.log("date long: ", dateLong);
    // console.log(date.getDate(), "==",  ("0" + date.getDate()).slice(-2));
    // console.log(date.getMonth() + 1,"==",("0" + (date.getMonth() + 1)).slice(-2));
    // console.log(date.getFullYear());
    
    let result = format
        .replace("dd", ("0" + date.getDate()).slice(-2))
        .replace("MM",  ("0" + (date.getMonth() + 1)).slice(-2))
        .replace("yyyy", date.getFullYear());
    return result;

}