export function showErrorMessage(response) {
    console.log(">>>> ", response.responseText);
    try {
        let errorObject = JSON.parse(response.responseText);
        let errorList = errorObject.error;
        let message = errorList
        if (Array.isArray(errorList)) {
            message = errorList.join('\n');
        }
        console.log(message);
        alert(message);
    } catch (error) {
        alert(response.responseText);
    }
}